package Practical02;

public class LinearInterpolation {

    public static int[] resample(int[] datapoints){

        int[] result = new int[datapoints.length*2-1];

        for(int i = 0; i < result.length; i++){

            if(i%2 == 0){

                result[i] = datapoints[i/2];

            } else {

                result[i] = (int)((datapoints[(i-1)/2] + datapoints[(i+1)/2]) / 2);

            }

        }

        return result;


    }

    public static int[] resample(int[] datapoints, int scale){

        int[] result = new int[datapoints.length*scale - scale+1];

        for(int i = 0; i < result.length; i+=scale){

            result[i] = datapoints[i/scale];

        }

        for(int i = 0; i < result.length; i++){

            if(i%scale == 0){ continue; }

            int x0 = i-(i%scale);
            int y0 = result[x0];

            int x1 = x0+scale;
            int y1 = result[x1];

            result[i] = LinearInterpolation.lerp(new Vector2(x0, y0), new Vector2(x1, y1), i);

        }

        return result;

    }

    public static int lerp(Vector2 v0, Vector2 v1, int x){

        int y;

        int numerator = (v1.y-v0.y) * (x - v0.x);
        int denom = v1.x - v0.x;

        y = (numerator / denom) + v0.y;

        return y;

    }

    public static int[][] resample(int[][] image){

        int[][] result = new int[image.length*2-1][image[0].length*2-1];

        for(int i = 0; i < result.length; i += 2){

            for(int j = 0; j < result[i].length; j += 2){

                result[i][j] = image[i/2][j/2];

            }

        }

        for(int i = 0; i < result.length; i+=2){

            for(int j = 1; j < result[i].length; j += 2){

                int x0 = j-(j%2);
                int y0 = result[i][x0];

                int x1 = x0+2;
                int y1 = result[i][x1];

                result[i][j] = LinearInterpolation.lerp(new Vector2(x0, y0), new Vector2(x1, y1), j);


            }

        }


        for(int j = 0; j < result[0].length; j++){

            for(int i = 1; i < result.length-1; i += 2){

                int x0 = i-(i%2);
                int y0 = result[x0][j];

                int x1 = x0+2;
                int y1 = result[x1][j];

                result[i][j] = LinearInterpolation.lerp(new Vector2(x0, y0), new Vector2(x1, y1), i);

            }

        }

        return result;


    }

    public static int[][] resample(int[][] image, int scale){


        int[][] result = new int[image.length*scale-scale+1][image[0].length*scale-scale+1];

        for(int i = 0; i < result.length; i+=scale){

            for(int j = 0; j < result[i].length; j+=scale){

                result[i][j] = image[i/scale][j/scale];

            }

        }


        for(int i = 0; i < result.length; i+=scale){

            for(int j = 0; j < result[i].length; j++){

                if(j%scale == 0){

                    continue;

                }

                int x0 = j-(j%scale);
                int y0 = result[i][x0];

                int x1 = x0+scale;
                int y1 = result[i][x1];

                result[i][j] = LinearInterpolation.lerp(new Vector2(x0, y0), new Vector2(x1, y1), j);


            }

        }


        for(int j = 0; j < result[0].length; j++){

            for(int i = 1; i < result.length-1; i++){

                if( i%scale==0 ){ continue; }

                int x0 = i-(i%scale);
                int y0 = result[x0][j];

                int x1 = x0+scale;
                int y1 = result[x1][j];

                result[i][j] = LinearInterpolation.lerp(new Vector2(x0, y0), new Vector2(x1, y1), i);

            }

        }

        return result;
       

    }

    
    public static void show(int[][] arr){

        for(int[] row : arr){

            for(int val : row){

                System.out.print(val + " ");

            }

            System.out.println("");

        }

    }
    
}