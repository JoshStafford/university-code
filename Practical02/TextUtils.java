package Practical02;

public class TextUtils {

    public static String[] split(String text){

        String current_word = "";
        String[] result = new String[TextUtils.charCount(text, ' ') + 1];

        for(int i = 0; i < text.length(); i++){

            if(text.charAt(i) == ' '){

                TextUtils.push(current_word, result);
                current_word = "";

            } else {

                current_word += text.charAt(i);

            }

        }

        if(current_word != ""){
            TextUtils.push(current_word, result);
        }

        return result;

    }

    public static void push(String text, String[] arr){

        for(int i = 0; i < arr.length; i++){

            if(arr[i] == null){
                arr[i] = text;
                break;
            } else {
                continue;
            }

        }

    }


    public static void row_push(int[] row, int[][] arr){

        for(int i = 0; i < arr.length; i++){

            if(arr[i] == null){
                arr[i] = row;
                break;
            } else {
                continue;
            }

        }

    }


    public static void int_push(int val, int[] arr){

        for(int i = 0; i < arr.length; i++){

            if(arr[i] == 0){
                arr[i] = val;
                break;
            } else {
                continue;
            }

        }

    }

    public static int charCount(String text, char target){

        int result = 0;

        for(int i = 0; i < text.length(); i++){

            if(text.charAt(i) == target){ result++; }

        }

        return result;

    }

    public static String[] split(String text, String separators){

        String current_word = "";
        int total_splits = 1;

        for(int i = 0; i < separators.length(); i++){

            total_splits += TextUtils.charCount(text, separators.charAt(i));

        }

        String[] result = new String[total_splits];

        for(int i = 0; i < text.length(); i++){

            if(TextUtils.in(text.charAt(i), separators)){

                TextUtils.push(current_word, result);
                current_word = "";

            } else {

                current_word += text.charAt(i);

            }

        
        }

        if(current_word != ""){

            TextUtils.push(current_word, result);

        }

        return result;


    }

    public static boolean in(char character, String separators){

        for(int i = 0; i < separators.length(); i++){

            if(character == separators.charAt(i)){
                return true;
            }

        }

        return false;

    }


    public static int[][] rasterize(int[] data, int width){

        if(data.length % width != 0){
            return null;
        }

        int[][] result = new int[data.length / width][width];
        int[] current_row = new int[width];

        for(int i = 0; i < data.length; i++){

            if(i%width == 0 && i != 0){

                result[(i-(i%width)) / width - 1] = current_row;    
                current_row = new int[width];

            } 

            TextUtils.int_push(data[i], current_row);


        }

        
        result[result.length-1] = current_row;

        return result;

    }


}