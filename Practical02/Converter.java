package Practical02;
import java.lang.Math;

public class Converter {

    public static int toBase10(String to_conv){

        String[] binChars = to_conv.split("");
        int total = 0;

        for(int i = 0; i < binChars.length; i++){

            int value = (int)Math.pow((double)2, (double)i); 

            total += value * Integer.parseInt(binChars[binChars.length-1-i]);

        }

        return total;

    }

}