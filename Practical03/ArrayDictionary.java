package Practical03;

public class ArrayDictionary {

    Integer[] keys;
    String[] values;
    int expand_size;

    public ArrayDictionary(){

        this.expand_size = 100;
        this.keys = new Integer[100];
        this.values = new String[100];

    }

    public ArrayDictionary(int size){

        this.expand_size = size;
        this.keys = new Integer[size];
        this.values = new String[size];

    }

    public String put(Integer key, String value){

        for(int i = 0; i < this.keys.length; i++){

            if(this.keys[i] == key){
                String old = this.values[i];
                this.values[i] = value;
                return old;
            }

        }

        for(int i = 0; i < this.keys.length; i++){

            if(this.keys[i] == null){

                this.keys[i] = key;
                this.values[i] = value;

                if(this.keys.length - i <= 5){ this.expand(); }

                break;

            }

        }

        return null;

    }

    public boolean contains(Integer key){

        for(Integer val : this.keys){

            if(key == val){ return true; }

        }

        return false;

    }

    public boolean isEmpty(){

        for(Integer val : this.keys){

            if(val != null){ return false; }

        }

        return true;

    }

    public String get(Integer key){

        for(int i = 0; i < this.keys.length; i++){

            if(this.keys[i] == key){
                return this.values[i];
            }

        }

        return null;
        
    }

    private void expand(){

        Integer[] new_keys = new Integer[this.keys.length+this.expand_size];
        String[] new_values = new String[this.values.length+this.expand_size];

        for(int i = 0; i < this.keys.length; i++){

            new_keys[i] = this.keys[i];
            new_values[i] = this.values[i];

        }

        this.keys = new_keys;
        this.values = new_values;

    }
    
    public String toString(){

        String result = "{ ";

        for(int i = 0; i < this.keys.length; i++){

            if(this.values[i] != null){

                String rec = String.format("%d : %s, ", this.keys[i], this.values[i]);
                result += rec;

            }

        }

        result = result.substring(0, result.length()-2);
        result += " }";

        return result;

    }

}