package Practical03;

public class Vector {

    private double[] _vector;
    
    public Vector(double[] vals){

        this._vector = vals;

    }

    public String toString(){

        String str = String.format("[ %f, %f, %f ]", this._vector[0], this._vector[1], this._vector[2]);

        return str;

    }

    public int size(){

        return this._vector.length;

    }

    public double get(int index){

        if(index >= this._vector.length){ throw new IndexOutOfBoundsException(); }

        return this._vector[index];

    }

    public void set(int index, double val){

        if(index >= this._vector.length){ throw new IndexOutOfBoundsException(); }

        this._vector[index] = val;

    }

    public Vector scalarProduct(double scalar){

        double[] vals = { this._vector[0]*scalar, this._vector[1]*scalar, this._vector[2]*scalar };

        return new Vector(vals);

    }

    public Vector add(Vector other){

        if(!(other instanceof Vector)){ return null; }
        if(this._vector.length != other.size()){ return null; }

        double[] vals = { this._vector[0] + other.get(0), this._vector[1] + other.get(1), this._vector[2] + other.get(2) };

        return new Vector(vals);

    }

    public boolean equals(Object other){

        if(other == null) { return false; }
        if(!(other instanceof Vector)){ return false; }
        Vector v = (Vector)other; 
        if(this == other){ return true; }
        if(this._vector.length != v.size()){ return false; }

        for(int i = 0; i < this._vector.length; i++){

            if(this._vector[i] != v.get(i)){ return false; }

        }

        return true;

    }

}